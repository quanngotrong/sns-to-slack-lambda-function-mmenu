const Slack = require('slack-node');
const moment = require('moment-timezone');
const defaultTimeZone = 'Asia/Ho_Chi_Minh';

// setting
const mmenuWebhookUri = '';
const CHANNEL = '';

const formatDate = (datetime, format) => {
  if (!datetime) {
    return '';
  }
  moment.locale('en');
  return moment(datetime).tz(defaultTimeZone).format(format);
};

const formatDateTimeYyyymmddHHmmss = (dateTime) => formatDate(dateTime, 'YYYY-MM-DD HH:mm:ss');

const sendMessage = (message) => {
  try {
    const slack = new Slack();
    slack.setWebhook(mmenuWebhookUri);
    const curdate = formatDateTimeYyyymmddHHmmss(new Date());
    slack.webhook(
      {
        channel: CHANNEL,
        username: 'mmenu-bot',
        icon_emoji: ':robot_face:',
        text: `${curdate} : ${message}`,
      },
      (err) => {
        console.log(err);
      }
    );
  } catch (error) {
    console.log(error);
  }
};

exports.handler = async (event) => {
  const promise = new Promise(function(resolve, reject) {
    const message = `Sns message: ${event.Records[0].Sns.Message || ''} `;
    sendMessage(message);
  });
  return promise;
};

