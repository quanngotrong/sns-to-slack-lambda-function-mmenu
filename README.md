cách sử dụng:
- fill các trường: mmenuWebhookUri, CHANNEL trong file index.js
- compress file dưới dạng zip rồi upload lên lambda
- đặt trigger là sns mong muốn

Vd:

2022-10-04 15:01:43 : Sns message: {"AlarmName":"mmenu-service-test-alarm","AlarmDescription":null,"AWSAccountId":"454700908687","AlarmConfigurationUpdatedTimestamp":"2022-10-04T07:17:25.280+0000","NewStateValue":"ALARM","NewStateReason":"Threshold Crossed: 1 out of the last 1 datapoints [34.588182140948874 (04/10/22 08:00:00)] was greater than the threshold (5.0) (minimum 1 datapoint for OK -> ALARM transition).","StateChangeTime":"2022-10-04T08:01:42.789+0000","Region":"Asia Pacific (Singapore)","AlarmArn":"arn:aws:cloudwatch:ap-southeast-1:454700908687:alarm:mmenu-service-test-alarm","OldStateValue":"OK","OKActions":[],"AlarmActions":["arn:aws:sns:ap-southeast-1:454700908687:test-topic-410"],"InsufficientDataActions":[],"Trigger":{"MetricName":"CPUUtilization","Namespace":"AWS/ECS","StatisticType":"Statistic","Statistic":"AVERAGE","Unit":null,"Dimensions":[{"value":"mmenu-service-test","name":"ServiceName"},{"value":"mmenu-test-cluster","name":"ClusterName"}],"Period":60,"EvaluationPeriods":1,"DatapointsToAlarm":1,"ComparisonOperator":"GreaterThanThreshold","Threshold":5.0,"TreatMissingData":"missing","EvaluateLowSampleCountPercentile":""}}